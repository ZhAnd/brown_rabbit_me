/* // A simple JavaScript Statement 
alert("HelloWorld");  

// First we declare a variable, and then perform an "IF" statement with that variable
var amount = 500;

if (amount < 1000) {
    alert("It's less than 1000");
}

// If statement with "Else" ( condition ? true : false )
var playerOne = 500;
var playerTwo = 600;

var highScore;

if(playerOne > playerTwo) {
    highScore = playerOne;
}
else{
    highScore = playerTwo;
}

// Debuging with firebug

var foo = 10;
var bar = 20;

if( foo < bar) {
    console.log("foo is less than a bar:");
    console.log(foo);
    console.log(bar);
}

// A while loop, a++ means that the var "a" will be increased by 1 with every iteration , until it becomes 10
// Without "a++", this will be an infinite loop

var a = 1;

while ( a < 10) {
    alert(a);
    a++;
}

*/